package nullapp

import (
	"reflect"
)

type Diagram struct {
	Id         string `json:"diagramId,omitempty"`
	Title      string `json:"title,omitempty"`
	Image      string `json:"imageUrl,omitempty"`
	Contact    string `json:"contact,omitempty"`
	URL        string `json:"url,omitempty"`
	Edited     bool   `json:"edited,omitempty"`
	Created    string `json:"created,omitempty"`
	LastUpdate string `json:"last_update,omitempty"`
}

type Diagrams []Diagram

func (d *Diagram) IsEmpty() bool {
	return reflect.DeepEqual(*d, Diagram{})
}
