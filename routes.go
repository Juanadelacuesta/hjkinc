package nullapp

import (
	"net/http"
)

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

func (app *nullApp) GetRoutes() Routes {
	app.routes = Routes{
		Route{
			"Dashboard",
			"GET",
			"/projects",
			app.Index,
		},
		Route{
			"Project",
			"GET",
			"/project/{id}",
			app.ShowProject,
		},
		Route{
			"HealthCheck",
			"GET",
			"/health",
			app.HealthCheck,
		},
	}
	return app.routes
}
