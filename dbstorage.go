package nullapp

import (
	"database/sql"

	_ "github.com/go-sql-driver/mysql"
)

// Mysql implementation of the DB interface for the projects storage

type DB struct {
	user,
	password,
	port,
	host,
	name string
}

func scanRow(rows *sql.Rows) Project {
	p := Project{}
	err := rows.Scan(&p.Id, &p.Title, &p.Description, &p.ProjectFolder, &p.Contact, &p.Client, &p.Created, &p.LastUpdate)
	if err != nil {
		panic(err.Error())
	}
	return p
}

func (m *DB) dbConn() (db *sql.DB) {

	db, err := sql.Open("mysql", m.user+":"+m.password+"@tcp("+m.host+":"+m.port+")/"+m.name+"?parseTime=true")
	if err != nil {
		panic(err.Error())
	}
	return db
}

func NewProjectStorage(dbUser, dbPass, dbName, dbPort, dbHost string) *DB {
	m := &DB{
		user:     dbUser,
		password: dbPass,
		name:     dbName,
		host:     dbHost,
		port:     dbPort,
	}

	db := m.dbConn()
	defer db.Close()
	if _, err := db.Exec(`
CREATE TABLE IF NOT EXISTS projects (
    id             INT AUTO_INCREMENT PRIMARY KEY,
    title          VARCHAR(100) NOT NULL,
    description    VARCHAR(1000) NOT NULL,
    projectfolder  VARCHAR(100) NOT NULL,
    contact        VARCHAR(100) NOT NULL,
    client         VARCHAR(100) NOT NULL,
    created        TIMESTAMP NOT NULL DEFAULT NOW(),
    lastupdate     TIMESTAMP NOT NULL DEFAULT NOW() ON UPDATE NOW());
`); err != nil {
		panic(err.Error())
	}
	return m
}

func (m *DB) GetAll() (interface{}, error) {
	db := m.dbConn()
	defer db.Close()

	ps := Projects{}
	rows, err := db.Query("SELECT * FROM projects ORDER BY id DESC")
	if err != nil {
		return ps, err
	}

	for rows.Next() {
		ps = append(ps, scanRow(rows))
	}
	return ps, nil
}

func (m *DB) GetById(Id string) interface{} {
	db := m.dbConn()
	defer db.Close()
	row, err := db.Query("SELECT * FROM projects WHERE id=?", Id)
	if err != nil {
		panic(err.Error())
	}

	result := row.Next()
	if result == false {
		return Project{}
	}

	p := scanRow(row)
	return p
}

func (m *DB) GetCount() (count int, err error) {
	db := m.dbConn()
	defer db.Close()

	row := db.QueryRow("SELECT count(*) FROM projects")
	err = row.Scan(&count)
	if err != nil {
		return -1, err
	}
	return count, nil
}

func (m *DB) Insert(i interface{}) (int64, error) {
	p, _ := i.(Project)
	db := m.dbConn()
	defer db.Close()

	r, err := db.Exec("INSERT INTO projects (title, description, projectfolder,contact, client) VALUES(?,?,?,?,?)", p.Title, p.Description, p.ProjectFolder, p.Contact, p.Client)
	if err != nil {
		return -1, err
	}
	id, _ := r.LastInsertId()
	return id, nil
}

func (m *DB) TruncTableProjects() error {
	db := m.dbConn()
	defer db.Close()

	if _, err := db.Exec("TRUNCATE TABLE projects"); err != nil {
		return err
	}
	return nil
}
