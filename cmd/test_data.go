package main

import (
	"github.com/Juanadelacuesta/nullapp"
)

const owner = "Test Owner"

var testDiagram1 = nullapp.Diagram{
	Id:         "123",
	Title:      "testDiagram 1",
	Image:      "Image Url",
	Contact:    owner,
	URL:        "diagram url",
	Edited:     true,
	Created:    "0-0-0000",
	LastUpdate: "0-0-0000",
}

var testDiagram2 = nullapp.Diagram{
	Id:         "122",
	Title:      "testDiagram 2",
	Image:      "Image Url",
	Contact:    "Dummy diagram owner",
	URL:        "diagram url",
	Edited:     true,
	Created:    "0-0-0000",
	LastUpdate: "0-0-0000",
}

var testProject1 = nullapp.Project{
	Title:         "Test Project 1",
	Description:   "Test Project 1 Decription",
	ProjectFolder: "Test Folder",
	Contact:       owner,
	Client:        "test Client",
}

var testProject2 = nullapp.Project{
	Title:         "Test Project 2",
	Description:   "Test Project 2 Description",
	ProjectFolder: "Test Folder",
	Contact:       "Dummy project owner",
	Client:        "test Client",
}
