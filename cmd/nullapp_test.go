package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"github.com/Juanadelacuesta/nullapp"
)

//Storage implementation avoid calls to Cacoo Api when testing
type testNl struct{}

func (db testNl) GetAll() (interface{}, error) {
	return nullapp.Diagrams{testDiagram1, testDiagram2}, nil
}
func (db testNl) GetById(Id string) interface{} {
	return nil
}
func (db testNl) GetCount() (int, error) {
	return 0, nil
}
func (db testNl) Insert(interface{}) (int64, error) {
	return 0, nil
}

func TestNullApp(t *testing.T) {
	dbUser := os.Getenv("DB_USER")
	dbPass := os.Getenv("DB_PASSWORD")
	dbName := os.Getenv("DB_NAME")
	dbPort := os.Getenv("DB_PORT")
	dbHost := os.Getenv("DB_HOST")
	db := nullapp.NewProjectStorage(dbUser, dbPass, dbName, dbPort, dbHost)
	s := testNl{}

	app := nullapp.New(dbUser, dbPass, dbName, dbPort, dbHost, "", nullapp.WithDiagramsStorage(s))
	ts := httptest.NewServer(app.Router)

	var cases = []struct {
		project        nullapp.Project
		P1Expected     bool
		P2Expected     bool
		expectedStatus int
		TD1Expected    bool
		TD2Expected    bool
	}{

		{
			testProject1,
			true,
			false,
			200,
			true,
			false,
		},
		{
			testProject2,
			true,
			true,
			200,
			false,
			false,
		},
	}

	if err := db.TruncTableProjects(); err != nil {
		t.Fatalf("error in the database: %s ", err.Error())
	}

	for i, tt := range cases {
		projectId, _ := db.Insert(tt.project)
		resp, err := http.Get(ts.URL + "/projects")
		if err != nil {
			t.Fatalf("Problem getting to the index in tests case %d", i)
		}
		if resp.StatusCode != http.StatusOK {
			t.Fatalf("in test case %d expected code 200, got %d", i, resp.StatusCode)
		}

		b, _ := ioutil.ReadAll(resp.Body)
		defer resp.Body.Close()

		body := string(b)
		if strings.Contains(body, testProject1.Title) != tt.P1Expected {
			t.Fatalf("in test case %d getting wrong project", i)
		}

		if strings.Contains(body, testProject2.Title) != tt.P2Expected {
			t.Fatalf("in test cass %d getting wrong project", i)
		}

		if !strings.Contains(body, testDiagram1.Title) || !strings.Contains(body, testDiagram2.Title) {
			t.Fatalf("getting wrong diagram information in test case %d", i)
		}

		resp, err = http.Get(fmt.Sprintf("%s/project/%d", ts.URL, projectId))
		if err != nil {
			t.Fatalf("Problem getting to the show in tests case %d", i)
		}

		if resp.StatusCode != tt.expectedStatus {
			t.Fatalf("expected code %d, got %d", tt.expectedStatus, resp.StatusCode)
		}

		b, _ = ioutil.ReadAll(resp.Body)
		defer resp.Body.Close()

		body = string(b)
		if !strings.Contains(body, tt.project.Description) {
			t.Fatalf("in test cass %d not getting project description", i)
		}

		if strings.Contains(body, testDiagram1.Title) != tt.TD1Expected {
			t.Fatalf("getting wrong diagram information in test case %d for TD1", i)
		}

		if strings.Contains(body, testDiagram2.Title) != tt.TD2Expected {
			t.Fatalf("getting wrong diagram information in test case %d for TD2", i)
		}
	}
}
