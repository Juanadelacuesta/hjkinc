package main

import (
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/Juanadelacuesta/nullapp"
)

func main() {
	apiKey := os.Getenv("CACOO_APIKEY")
	dbUser := os.Getenv("DB_USER")
	dbPass := os.Getenv("DB_PASSWORD")
	dbName := os.Getenv("DB_NAME")
	dbPort := os.Getenv("DB_PORT")
	dbHost := os.Getenv("DB_HOST")
	port := os.Getenv("APP_PORT")
	app := nullapp.New(dbUser, dbPass, dbName, dbPort, dbHost, apiKey)

	srv := &http.Server{
		Handler:      app.Router,
		Addr:         fmt.Sprintf(":%s", port),
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	srv.ListenAndServe()
}
