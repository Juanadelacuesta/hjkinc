package nullapp

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

// Api calls implementation to get the diagrams imformation

type NulabClient struct {
	apiKey string
	client *http.Client
}

type Result map[string]interface{}

type Results struct {
	Results []Result `json:"result,omitempty"`
	Count   int      `json:"count,omitempty"`
}

func NewDiagramsStorage(apiKey string) Db {
	return &NulabClient{
		apiKey: apiKey,
		client: &http.Client{
			Timeout: time.Second * 10,
		},
	}
}

func fillDiagram(result Result) Diagram {
	return Diagram{
		Id:         result["diagramId"].(string),
		Title:      result["title"].(string),
		Image:      result["imageUrl"].(string),
		Contact:    result["ownerNickname"].(string),
		URL:        result["url"].(string),
		Edited:     result["editing"].(bool),
		Created:    result["created"].(string),
		LastUpdate: result["updated"].(string),
	}
}

func getResults(respBody []byte) (Diagrams, error) {
	r := Results{}
	ds := Diagrams{}
	err := json.Unmarshal(respBody, &r)
	if err != nil {
		return ds, err
	}

	for _, r := range r.Results {
		ds = append(ds, fillDiagram(r))
	}
	return ds, nil
}

func (c *NulabClient) GetAll() (interface{}, error) {
	url := fmt.Sprintf("https://cacoo.com/api/v1/diagrams.json?apiKey=%s", c.apiKey)
	resp, _ := c.client.Get(url)
	ds := Diagrams{}

	if resp.StatusCode != http.StatusOK {
		return &ds, fmt.Errorf("http error with Cacoo: %s", resp.Status)
	}

	bodyBytes, _ := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()

	ds, err := getResults(bodyBytes)
	if err != nil {
		return &ds, fmt.Errorf("http error with Cacoo: %s", err.Error())
	}
	return ds, nil
}
func (c *NulabClient) GetById(Id string) interface{} {
	return nil
}

func (c *NulabClient) GetCount() (int, error) {
	return 0, nil
}

func (c *NulabClient) Insert(interface{}) (int64, error) {
	return 0, nil
}
