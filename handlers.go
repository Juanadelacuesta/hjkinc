package nullapp

import (
	"html/template"
	"net/http"

	"github.com/gorilla/mux"
)

type indexData struct {
	P Projects
	D Diagrams
}

type showData struct {
	P Project
	D Diagrams
}

func filterDiagramsByProject(owner string, ds Diagrams) Diagrams {
	fds := Diagrams{}
	for _, d := range ds {
		if d.Contact == owner {
			fds = append(fds, d)
		}
	}
	return fds
}

func (app *nullApp) Index(w http.ResponseWriter, r *http.Request) {
	tmpl := template.Must(template.ParseFiles("../templates/index.gohtml"))
	p, err := app.projects.GetAll()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	d, err := app.diagrams.GetAll()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	data := indexData{
		P: p.(Projects),
		D: d.(Diagrams),
	}

	if err := tmpl.ExecuteTemplate(w, "index.gohtml", data); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (app *nullApp) ShowProject(w http.ResponseWriter, r *http.Request) {
	tmpl := template.Must(template.ParseFiles("../templates/show.gohtml"))
	p := app.projects.GetById(mux.Vars(r)["id"]).(Project)
	if p.IsEmpty() {
		http.Error(w, "The project your are looking for doesn't exist", http.StatusNotFound)
		return
	}

	d, err := app.diagrams.GetAll()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	data := showData{
		P: p,
		D: filterDiagramsByProject(p.Contact, d.(Diagrams)),
	}

	if err := tmpl.ExecuteTemplate(w, "show.gohtml", data); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (app *nullApp) HealthCheck(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusNoContent)
}
