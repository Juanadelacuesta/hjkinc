# HJKInc Dashboard

This project consists of an API that accepts SMS messages submitted via a POST request containing a JSON object as request body and posts them to the MessageBird.

The API has two endpoints:

| Name          | Action           | Method    | Path                    |
| ------------- | :--------------: | :-------: | :---------------------: |
| Dasboard      | Index            | GET       | "/projects"             |
| Project Pag   | Show             | GET       | "/project/id"           |
| Health Check  | HealthCheck      | GET       | "/health"               |

In the dasboard you can see all the projects of the company, and on the right side all the wireframes the designers are working on.
In the project page you can see more information on the project and all the wireframes that belong to that project.

* **The important thing in this project is the backend, the frontend is very simple and its only purpose is to make the backend more visible, the information about the company projects is stored in a Mysql database, 
while the information for the diagrams comes directly from the Cacoo Api using an ApiKey authentication. 
This projects is for demostration purposes only.**

## Getting Started

To run the server go to the cmd folder and execute the cmd, or run it with go or build it and execute it:

```
go build && ./cmd
```

In order to run it and contribute you need to replace the import paths in the main file and tests to your workspace.

### Prerequisites

You need to provide the following variables for configuration:

* DB_NAME: Name of the database
* DB_USER: User with provileges to read/write the database
* DB_PASSWORD: Password for the DB_USER
* DB_PORT: Port used to connect to the database
* DB_HOST: Host where the database is running
* APP_PORT: Port where you want you app to be

In order to contribute you need to download the package [Gorilla Mux](https://github.com/gorilla/mux) and the driver for [Mysql](https://github.com/go-sql-driver/mysql)

## Running the tests

To run the test go to the project folder and run, if you want to use a different apiKey for testing or a different DB, 
set the env variables to the test values.

```
go test
```


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
