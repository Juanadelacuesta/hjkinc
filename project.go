package nullapp

import (
	"reflect"
)

type Project struct {
	Id            int64  `json:"id,omitempty"`
	Title         string `json:"title,omitempty"`
	Description   string `json:"description,omitempty"`
	ProjectFolder string `json:"project_folder,omitempty"`
	Contact       string `json:"contact,omitempty"`
	Client        string `json:"client,omitempty"`
	Created       string `json:"created,omitempty"`
	LastUpdate    string `json:"last_update,omitempty"`
}

type Projects []Project

func (p *Project) IsEmpty() bool {
	return reflect.DeepEqual(*p, Project{})
}
