package nullapp

import (
	"github.com/gorilla/mux"
	"net/http"
)

type Db interface {
	GetAll() (interface{}, error)
	GetById(Id string) interface{}
	GetCount() (int, error)
	Insert(interface{}) (int64, error)
}

type nullApp struct {
	routes   Routes
	Router   http.Handler
	projects Db
	diagrams Db
}

type Option func(*nullApp)

func (n *nullApp) Projects() Db {
	return n.projects
}

func WithDiagramsStorage(s Db) Option {
	return func(app *nullApp) {
		app.diagrams = s
	}
}

func WithProjectStorage(s Db) Option {
	return func(app *nullApp) {
		app.projects = s
	}
}

func newRouter(routes Routes) *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	for _, route := range routes {
		router.Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(route.HandlerFunc)
	}

	router.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("../static/"))))

	return router
}
func New(dbUser, dbPass, dbName, dbPort, dbHost, apiKey string, options ...Option) *nullApp {
	b := &nullApp{}
	b.Router = newRouter(b.GetRoutes())
	b.projects = NewProjectStorage(dbUser, dbPass, dbName, dbPort, dbHost)
	b.diagrams = NewDiagramsStorage(apiKey)

	for _, opt := range options {
		opt(b)
	}
	return b
}

func (app *nullApp) Start() {
}
